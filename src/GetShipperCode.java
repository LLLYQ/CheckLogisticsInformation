import java.util.HashMap;
import java.util.Map;

/**
 * Created by LYQ on 2017/10/24 0024.
 * 快递的简称
 */
public class GetShipperCode {

    public static String getShipperCode(String expresseName){
        String ShipperCode = "";
        Map<String,String> map = new HashMap<>();
        map.put("安捷快递","AJ");
        map.put("安能物流","ANE");
        map.put("安信达快递","AXD");
        map.put("北青小红帽","BQXHM");
        map.put("百福东方","BFDF");
        map.put("百世快运","BTWL");
        map.put("CCES快递","CCES");
        map.put("城市100","CITY100");
        map.put("COE东方快递","COE");
        map.put("长沙创一","CSCY");
        map.put("成都善途速运","CDSTKY");
        map.put("德邦","DBL");
        map.put("德邦物流","DBL");
        map.put("D速物流","DSWL");
        map.put("大田物流","DTWL");
        map.put("EMS","EMS");
        map.put("快捷速递","FAST");
        map.put("FEDEX联邦(国内件)","FEDEX");
        map.put("FEDEX联邦(国际件)","FEDEX_GJ");
        map.put("飞康达","FKD");
        map.put("广东邮政","GDEMS");
        map.put("共速达","GSD");
        map.put("国通快递","GTO");
        map.put("高铁速递","GTSD");
        map.put("汇丰物流","HFWL");
        map.put("天天快递","HHTT");
        map.put("恒路物流","HLWL");
        map.put("天地华宇","HOAU");
        map.put("华强物流","hq568");
        map.put("百世快递","HTKY");
        map.put("百世汇通","HTKY");
        map.put("华夏龙物流","HXLWL");
        map.put("好来运快递","HYLSD");
        map.put("京广速递","JGSD");
        map.put("九曳供应链","JIUYE");
        map.put("佳吉快运","JJKY");
        map.put("佳吉快递","JJKY");
        map.put("嘉里物流","JLDT");
        map.put("捷特快递","JTKD");
        map.put("急先达","JXD");
        map.put("晋越快递","JYKD");
        map.put("加运美","JYM");
        map.put("佳怡物流","JYWL");
        map.put("跨越物流","KYWL");
        map.put("龙邦快递","LB");
        map.put("龙邦速递","LB");
        map.put("联昊通速递","LHT");
        map.put("民航快递","MHKD");
        map.put("明亮物流","MLWL");
        map.put("能达速递","NEDA");
        map.put("平安达腾飞快递","PADTF");
        map.put("全晨快递","QCKD");
        map.put("全峰快递","QFKD");
        map.put("全日通快递","QRT");
        map.put("如风达","RFD");
        map.put("赛澳递","SAD");
        map.put("圣安物流","SAWL");
        map.put("盛邦物流","SBWL");
        map.put("上大物流","SDWL");
        map.put("顺丰快递","SF");
        map.put("盛丰物流","SFWL");
        map.put("盛辉物流","SHWL");
        map.put("速通物流","ST");
        map.put("申通快递","STO");
        map.put("速腾快递","STWL");
        map.put("速尔快递","SURE");
        map.put("唐山申通","TSSTO");
        map.put("全一快递","UAPEX");
        map.put("优速快递","UC");
        map.put("万家物流","WJWL");
        map.put("万象物流","WXWL");
        map.put("新邦物流","XBWL");
        map.put("信丰快递","XFEX");
        map.put("希优特","XYT");
        map.put("新杰物流","XJ");
        map.put("源安达快递","YADEX");
        map.put("远成物流","YCWL");
        map.put("韵达快递","YD");
        map.put("义达国际物流","YDH");
        map.put("越丰物流","YFEX");
        map.put("原飞航物流","YFHEX");
        map.put("亚风快递","YFSD");
        map.put("运通快递","YTKD");
        map.put("圆通速递","YTO");
        map.put("圆通快递","YTO");
        map.put("亿翔快递","YXKD");
        map.put("邮政平邮","YZPY");
        map.put("邮政小包","YZPY");
        map.put("增益快递","ZENY");
        map.put("增益速递","ZENY");
        map.put("汇强快递","ZHQKD");
        map.put("宅急送","ZJS");
        map.put("宅急送快递","ZJS");
        map.put("众通快递","ZTE");
        map.put("中铁快运","ZTKY");
        map.put("中通快递","ZTO");
        map.put("中铁物流","ZTWL");
        map.put("中邮物流","ZYWL");
        map.put("亚马逊物流","AMAZON");
        map.put("速必达物流","SUBIDA");
        map.put("瑞丰速递","RFEX");
        map.put("快客快递","QUICK");
        map.put("城际快递","CJKD");
        map.put("CNPEX中邮快递","CNPEX");
        map.put("鸿桥供应链","HOTSCM");
        map.put("海派通物流公司","HPTEX");
        map.put("澳邮专线","AYCA");
        map.put("泛捷快递","PANEX");
        map.put("PCA Express","PCA");
        map.put("UEQ Express","UEQ");
        if (map.containsKey(expresseName)){
            ShipperCode = map.get(expresseName);
        }
        return ShipperCode;
    }
}
