package expresse.trackQuery;

/**
 * Created by LYQ on 2017/3/1 0001.
 * 即时查询  追踪详情
 */
public class Trace {

//    AcceptTime		时间
    private String AcceptTime;
//    AcceptStation		描述
    private String AcceptStation;
//    Remark	    	备注
    private String Remark;

    public String getAcceptTime() {
        return AcceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        AcceptTime = acceptTime;
    }

    public String getAcceptStation() {
        return AcceptStation;
    }

    public void setAcceptStation(String acceptStation) {
        AcceptStation = acceptStation;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }
}
