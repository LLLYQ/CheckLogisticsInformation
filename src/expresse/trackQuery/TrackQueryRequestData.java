package expresse.trackQuery;

/**
 * Created by LYQ on 2017/3/1 0001.
 * 即时查询请求参数
 */
public class TrackQueryRequestData {

    //OrderCode		订单编号
    private String OrderCode;
    //ShipperCode	快递公司编码
    private String ShipperCode;
    //LogisticCode	物流单号
    private String LogisticCode;

    public String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(String orderCode) {
        OrderCode = orderCode;
    }

    public String getShipperCode() {
        return ShipperCode;
    }

    public void setShipperCode(String shipperCode) {
        ShipperCode = shipperCode;
    }

    public String getLogisticCode() {
        return LogisticCode;
    }

    public void setLogisticCode(String logisticCode) {
        LogisticCode = logisticCode;
    }

}
