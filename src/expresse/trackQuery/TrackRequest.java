package expresse.trackQuery;

/**
 * Created by LYQ on 2017/3/2 0002.
 */
public class TrackRequest {

//    RequestData		请求内容需进行URL(utf-8)编码。请求内容JSON格式，须和DataType一致。
    private String RequestData;
//    EBusinessID		商户ID，请在我的服务页面查看。
    private String EBusinessID;
//    RequestType		请求指令类型：1002
    private String RequestType;
//    DataSign	    	数据内容签名：把(请求内容(未编码)+AppKey)进行MD5加密，然后Base64编码，最后 进行URL(utf-8)编码。详细过程请查看Demo。
    private String DataSign;
//    DataType	    	请求、返回数据类型：2-json；
    private String DataType;

    public String getRequestData() {
        return RequestData;
    }

    public void setRequestData(String requestData) {
        RequestData = requestData;
    }

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getRequestType() {
        return RequestType;
    }

    public void setRequestType(String requestType) {
        RequestType = requestType;
    }

    public String getDataSign() {
        return DataSign;
    }

    public void setDataSign(String dataSign) {
        DataSign = dataSign;
    }

    public String getDataType() {
        return DataType;
    }

    public void setDataType(String dataType) {
        DataType = dataType;
    }
}
