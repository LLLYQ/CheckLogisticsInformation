package expresse.trackQuery;

import java.util.List;
import java.util.Map;

/**
 * Created by LYQ on 2017/3/1 0001.
 * 即时查询返回参数
 */
public class TrackQueryReceiveData {

//    EBusinessID		用户ID
    private String EBusinessID;
//    OrderCode		订单编号
    private String OrderCode;
//    ShipperCode		快递公司编码
    private String ShipperCode;
//    LogisticCode		物流运单号
    private String LogisticCode;
//    Success		成功与否
    private boolean Success;
//    Reason		失败原因
    private String Reason;
//    State		物流状态：2-在途中,3-签收,4-问题件
    private String State;
//    Traces 追踪详情
    private List<Map<String,Trace>> Traces;

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(String orderCode) {
        OrderCode = orderCode;
    }

    public String getShipperCode() {
        return ShipperCode;
    }

    public void setShipperCode(String shipperCode) {
        ShipperCode = shipperCode;
    }

    public String getLogisticCode() {
        return LogisticCode;
    }

    public void setLogisticCode(String logisticCode) {
        LogisticCode = logisticCode;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public List<Map<String, Trace>> getTraces() {
        return Traces;
    }

    public void setTraces(List<Map<String, Trace>> traces) {
        Traces = traces;
    }
}
