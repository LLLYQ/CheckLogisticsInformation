package expresse.subscribeTrack;

import java.util.List;

/**
 * Created by LYQ on 2017/3/1 0001.
 * 物流跟踪请求参数
 */
public class SubscribeRequestData {

    //    CallBack		用户自定义回调信息
    private String CallBack;
    //    MemberID		会员标识(备用字段)
    private String MemberID;
    //    CustomerName	电子面单客户账号(与快递网点申请)
    private String CustomerName;
    //    CustomerPwd	电子面单密码
    private String CustomerPwd;
    //    SendSite		收件网点标识
    private String SendSite;
    //    ShipperCode	快递公司编码
    private String ShipperCode;
    //    LogisticCode	快递单号
    private String LogisticCode;
    //    OrderCode		订单编号
    private String OrderCode;
    //    MonthCode		月结编码
    private String MonthCode;
    //    PayType		邮费支付方式:1-现付，2-到付，3-月结，4-第三方支付
    private Integer PayType;
    //    ExpType		快递类型：1-标准快件
    private String ExpType;
    //    Cost	    	寄件费（运费）
    private Double Cost;
    //    OtherCost		其他费用
    private Double OtherCost;
    private Receiver receiver;
    private Sender sender;
    //    StartDate		上门取货时间段:"yyyy-MM-dd HH:mm:ss"格式化，本文中所有时间格式相同
    private String StartDate;
    private String EndDate;
    //    Weight		物品总重量kg
    private Double Weight;
    //    Quantity		件数/包裹数
    private Integer Quantity;
    //    Volume		物品总体积m3
    private Double Volume;
    //    Remark		备注
    private String String;
    //    IsNotice		是否分发到快递公司：1-不分发；0-分发.默认为0
    private Integer IsNotice;
    //    AddService(数组形式)
    private List<AddService> addServiceList;
    //    Commodity(数组形式)
    private List<Commodity> commodityList;

    public java.lang.String getCallBack() {
        return CallBack;
    }

    public void setCallBack(java.lang.String callBack) {
        CallBack = callBack;
    }

    public java.lang.String getMemberID() {
        return MemberID;
    }

    public void setMemberID(java.lang.String memberID) {
        MemberID = memberID;
    }

    public java.lang.String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(java.lang.String customerName) {
        CustomerName = customerName;
    }

    public java.lang.String getCustomerPwd() {
        return CustomerPwd;
    }

    public void setCustomerPwd(java.lang.String customerPwd) {
        CustomerPwd = customerPwd;
    }

    public java.lang.String getSendSite() {
        return SendSite;
    }

    public void setSendSite(java.lang.String sendSite) {
        SendSite = sendSite;
    }

    public java.lang.String getShipperCode() {
        return ShipperCode;
    }

    public void setShipperCode(java.lang.String shipperCode) {
        ShipperCode = shipperCode;
    }

    public java.lang.String getLogisticCode() {
        return LogisticCode;
    }

    public void setLogisticCode(java.lang.String logisticCode) {
        LogisticCode = logisticCode;
    }

    public java.lang.String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(java.lang.String orderCode) {
        OrderCode = orderCode;
    }

    public java.lang.String getMonthCode() {
        return MonthCode;
    }

    public void setMonthCode(java.lang.String monthCode) {
        MonthCode = monthCode;
    }

    public Integer getPayType() {
        return PayType;
    }

    public void setPayType(Integer payType) {
        PayType = payType;
    }

    public java.lang.String getExpType() {
        return ExpType;
    }

    public void setExpType(java.lang.String expType) {
        ExpType = expType;
    }

    public Double getCost() {
        return Cost;
    }

    public void setCost(Double cost) {
        Cost = cost;
    }

    public Double getOtherCost() {
        return OtherCost;
    }

    public void setOtherCost(Double otherCost) {
        OtherCost = otherCost;
    }

    public Receiver getReceiver() {
        return receiver;
    }

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public java.lang.String getStartDate() {
        return StartDate;
    }

    public void setStartDate(java.lang.String startDate) {
        StartDate = startDate;
    }

    public java.lang.String getEndDate() {
        return EndDate;
    }

    public void setEndDate(java.lang.String endDate) {
        EndDate = endDate;
    }

    public Double getWeight() {
        return Weight;
    }

    public void setWeight(Double weight) {
        Weight = weight;
    }

    public Integer getQuantity() {
        return Quantity;
    }

    public void setQuantity(Integer quantity) {
        Quantity = quantity;
    }

    public Double getVolume() {
        return Volume;
    }

    public void setVolume(Double volume) {
        Volume = volume;
    }

    public java.lang.String getString() {
        return String;
    }

    public void setString(java.lang.String string) {
        String = string;
    }

    public Integer getIsNotice() {
        return IsNotice;
    }

    public void setIsNotice(Integer isNotice) {
        IsNotice = isNotice;
    }

    public List<AddService> getAddServiceList() {
        return addServiceList;
    }

    public void setAddServiceList(List<AddService> addServiceList) {
        this.addServiceList = addServiceList;
    }

    public List<Commodity> getCommodityList() {
        return commodityList;
    }

    public void setCommodityList(List<Commodity> commodityList) {
        this.commodityList = commodityList;
    }

}
