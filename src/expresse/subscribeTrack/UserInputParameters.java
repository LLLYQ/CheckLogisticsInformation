package expresse.subscribeTrack;

/**
 * Created by LYQ on 2017/3/1 0001.
 */
public class UserInputParameters {

//    EBusinessID	用户电商ID
    private String EBusinessID;
//    PushTime		推送时间
    private String PushTime;
//    Count	    	推送物流单号轨迹个数
    private String Count;
//    Data	    	推送物流单号轨迹集合
    private String Data;

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getPushTime() {
        return PushTime;
    }

    public void setPushTime(String pushTime) {
        PushTime = pushTime;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
