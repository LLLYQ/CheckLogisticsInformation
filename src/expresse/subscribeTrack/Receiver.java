package expresse.subscribeTrack;

/**
 * Created by LYQ on 2017/3/1 0001.
 */
public class Receiver {

//    Company	收件人公司
    private String Company;
//    Name		收件人
    private String Name;
//    Tel		电话
    private String Tel;
//    Mobile	手机
    private String Mobile;
//    PostCode	收件人邮编
    private String PostCode;
//    ProvinceName		收件省（如广东省，不要缺少“省”）
    private String ProvinceName;
//    CityName	收件市（如深圳市，不要缺少“市”）
    private String CityName;
//    ExpAreaName	收件区（如福田区，不要缺少“区”或“县”）
    private String ExpAreaName;
//    Address	收件人详细地址
    private String Address;

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getExpAreaName() {
        return ExpAreaName;
    }

    public void setExpAreaName(String expAreaName) {
        ExpAreaName = expAreaName;
    }

    public String getProvinceName() {
        return ProvinceName;
    }

    public void setProvinceName(String provinceName) {
        ProvinceName = provinceName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getPostCode() {
        return PostCode;
    }

    public void setPostCode(String postCode) {
        PostCode = postCode;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String tel) {
        Tel = tel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
