package expresse.subscribeTrack;

import com.sun.org.apache.xpath.internal.operations.Bool;
import com.xiangshi.wzc.wx.expresse.trackQuery.Trace;

import java.util.Map;

/**
 * Created by LYQ on 2017/3/1 0001.
 */
public class SubscribeReturnData {

//    EBusinessID		商户ID
    private String EBusinessID;
//    OrderCode	    	订单编号
    private String OrderCode;
//    ShipperCode		快递公司编码
    private String ShipperCode;
//    LogisticCode		快递单号
    private String LogisticCode;
//    Success	    	成功与否：true,false
    private Bool Success;
//    Reason	    	失败原因
    private String Reason;
//    State	        	物流状态: 0-无轨迹，1-已揽收，2-在途中 201-到达派件城市，3-签收,4-问题件
    private String State;
//    CallBack	    	订阅接口的Bk值
    private String CallBack;
//    Traces
    private Map<String,Trace> Traces;
//    EstimatedDeliveryTime		预计到达时间yyyy-mm-dd
    private String EstimatedDeliveryTime;
    private PickerInfoData PickerInfo;
    private SenderInfoData SenderInfo;

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getOrderCode() {
        return OrderCode;
    }

    public void setOrderCode(String orderCode) {
        OrderCode = orderCode;
    }

    public String getShipperCode() {
        return ShipperCode;
    }

    public void setShipperCode(String shipperCode) {
        ShipperCode = shipperCode;
    }

    public String getLogisticCode() {
        return LogisticCode;
    }

    public void setLogisticCode(String logisticCode) {
        LogisticCode = logisticCode;
    }

    public Bool getSuccess() {
        return Success;
    }

    public void setSuccess(Bool success) {
        Success = success;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCallBack() {
        return CallBack;
    }

    public void setCallBack(String callBack) {
        CallBack = callBack;
    }

    public Map<String, Trace> getTraces() {
        return Traces;
    }

    public void setTraces(Map<String, Trace> traces) {
        Traces = traces;
    }

    public String getEstimatedDeliveryTime() {
        return EstimatedDeliveryTime;
    }

    public void setEstimatedDeliveryTime(String estimatedDeliveryTime) {
        EstimatedDeliveryTime = estimatedDeliveryTime;
    }

    public PickerInfoData getPickerInfo() {
        return PickerInfo;
    }

    public void setPickerInfo(PickerInfoData pickerInfo) {
        PickerInfo = pickerInfo;
    }

    public SenderInfoData getSenderInfo() {
        return SenderInfo;
    }

    public void setSenderInfo(SenderInfoData senderInfo) {
        SenderInfo = senderInfo;
    }
}
