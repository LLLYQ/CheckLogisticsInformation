package expresse.subscribeTrack;

/**
 * Created by LYQ on 2017/3/2 0002.
 */
public class PickerInfoData {

//    PersonName	    快递员姓名
    private String PersonName;
//    PersonTel	    	快递员电话
    private String PersonTel;
//    PersonCode		快递员工号
    private String PersonCode;
//    StationName		网点名称
    private String StationName;
//    StationAddress	网点地址
    private String StationAddress;
//    StationTel		网点电话
    private String StationTel;

    public String getPersonName() {
        return PersonName;
    }

    public void setPersonName(String personName) {
        PersonName = personName;
    }

    public String getPersonTel() {
        return PersonTel;
    }

    public void setPersonTel(String personTel) {
        PersonTel = personTel;
    }

    public String getPersonCode() {
        return PersonCode;
    }

    public void setPersonCode(String personCode) {
        PersonCode = personCode;
    }

    public String getStationName() {
        return StationName;
    }

    public void setStationName(String stationName) {
        StationName = stationName;
    }

    public String getStationAddress() {
        return StationAddress;
    }

    public void setStationAddress(String stationAddress) {
        StationAddress = stationAddress;
    }

    public String getStationTel() {
        return StationTel;
    }

    public void setStationTel(String stationTel) {
        StationTel = stationTel;
    }
}
