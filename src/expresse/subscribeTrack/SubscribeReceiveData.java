package expresse.subscribeTrack;

import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by LYQ on 2017/3/1 0001.
 * 物流跟踪请求参数
 */
public class SubscribeReceiveData {

//    EBusinessID		用户ID
    private String EBusinessID;
//    UpdateTime		时间
    private String UpdateTime;
//    Success		成功与否：true，false
    private Bool Success;
//    Reason		失败原因
    private String Reason;

    public String getEBusinessID() {
        return EBusinessID;
    }

    public void setEBusinessID(String EBusinessID) {
        this.EBusinessID = EBusinessID;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(String updateTime) {
        UpdateTime = updateTime;
    }

    public Bool getSuccess() {
        return Success;
    }

    public void setSuccess(Bool success) {
        Success = success;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }
}
