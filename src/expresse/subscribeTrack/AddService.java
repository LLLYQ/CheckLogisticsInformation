package expresse.subscribeTrack;

/**
 * Created by LYQ on 2017/3/1 0001.
 */
public class AddService {

//    Name		增值服务名称
    private String Name;
//    Value		增值服务值
    private String Value;
//    CustomerID	客户标识(选填)
    private String CustomerID;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public String getCustomerID() {
        return CustomerID;
    }

    public void setCustomerID(String customerID) {
        CustomerID = customerID;
    }
}
