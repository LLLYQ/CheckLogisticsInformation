package expresse.subscribeTrack;

/**
 * Created by LYQ on 2017/3/1 0001.
 */
public class Commodity {

//    GoodsName		商品名称
    private String GoodsName;
//    GoodsCode		商品编码
    private String GoodsCode;
//    Goodsquantity		件数
    private Integer Goodsquantity;
//    GoodsPrice	商品价格
    private Double GoodsPrice;
//    GoodsWeight	商品重量kg
    private Double GoodsWeight;
//    GoodsDesc		商品描述
    private String GoodsDesc;
//    GoodsVol		商品体积m3
    private Double GoodsVol;

    public String getGoodsName() {
        return GoodsName;
    }

    public void setGoodsName(String goodsName) {
        GoodsName = goodsName;
    }

    public Double getGoodsVol() {
        return GoodsVol;
    }

    public void setGoodsVol(Double goodsVol) {
        GoodsVol = goodsVol;
    }

    public String getGoodsDesc() {
        return GoodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        GoodsDesc = goodsDesc;
    }

    public Double getGoodsWeight() {
        return GoodsWeight;
    }

    public void setGoodsWeight(Double goodsWeight) {
        GoodsWeight = goodsWeight;
    }

    public Integer getGoodsquantity() {
        return Goodsquantity;
    }

    public void setGoodsquantity(Integer goodsquantity) {
        Goodsquantity = goodsquantity;
    }

    public Double getGoodsPrice() {
        return GoodsPrice;
    }

    public void setGoodsPrice(Double goodsPrice) {
        GoodsPrice = goodsPrice;
    }

    public String getGoodsCode() {
        return GoodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        GoodsCode = goodsCode;
    }
}
